from django.urls import path

from receipts.views import ReceiptListView
from django.contrib.auth import views as auth_views


#   RecipeCreateView,
#     RecipeDeleteView,
#     RecipeUpdateView,
#     log_rating,
#     RecipeDetailView,

urlpatterns = [
    path("login/", auth_views.LoginView.as_view(), name="login"),
    path("logout/", auth_views.LogoutView.as_view(), name="logout"),
    # path("<int:pk>/", RecipeDetailView.as_view(), name="recipe_detail"),
    # path("<int:pk>/delete/", RecipeDeleteView.as_view(), name="recipe_delete"),
    # path("new/", RecipeCreateView.as_view(), name="recipe_new"),
    # path("<int:pk>/edit/", RecipeUpdateView.as_view(), name="recipe_edit"),
    # path("<int:recipe_id>/ratings/", log_rating, name="recipe_rating"),
]
